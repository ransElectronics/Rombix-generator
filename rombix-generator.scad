//Number of different rombuses to enumerate.
rombCount = 6;
//Side length of each rhombus
rombSize = 20;
//Thickness of the pieces for 3d printing
rombHeight = 1.6;
//Radius of the fillet on the rombixes
rombFiletSize = 0.15;
//Extra space between each rombix
rombSpace = 0;
//Whether or not it generates the rombixes
generateRombixes = true;
//whether or not it generates the bed the pieces fit into
generateBed = true;
//Compensate for pieces overextruding
innerBedOffset=3;
module __Customizer_Limit__ () {}
rombAngle = 180/(rombCount*2);
$fn = 24;
eps = 0.01;


/*
The rombixes are enumerated as follows:
Draw an n by n grid, with n being 1/2 the number of sides of the equilateral 
polygon you want to fill. Along the x axis, you draw the top rhombus, with it 
being n even divisions between 0 and 180 degrees. Along the y axis, you draw the 
bottom rhombus with n even divisions between 0 and 180 degrees. Duplicate rombix
pieces are mirrored across both diagonal axes, through rotation and/or 
reflection. Pieces along the positive diagonal are chopped in half so they form 
a rombus instead of a parallelogram. This leaves four right triangles with their 
hypoteneuses along the original square's faces, that contain four equal sets of 
rombixes. The following code uses two sets of nested for loops to enumerate 
these, one for the bottom half ofthe left triangle aond one for the top half. 
The top half is then translated to fit in the negative space below and to the 
right of the bottom half. Here is a diagram of the transformation:
*-------*
|\     /|
|y\   / |
|yy\ /  |
|xxx*   | --> |xxx*
|xx/ \  |     |xx/|
|x/   \ |     |x/y|
|/     \|     |/yy|
*-------*     *---- 
*/
if (generateRombixes) {
//the bottom half of the aforementioned triangle
for(a=[1 : 1 : rombCount*2]) { 
    for(b=[a: 1 : rombCount]) {
        translate([(a)*rombSize*(2+rombSpace), b*rombSize*(2+rombSpace), 0]) {
            linear_extrude(height = rombHeight+rombFiletSize, center = false) {
                offset(r=rombFiletSize) { 
                    offset(r=-rombFiletSize) {
                    //Draw only a rombus where it would draw a 1x2 parallelogram.
                        if ( a==b ) { rhombus(180+a*rombAngle,rombSize); } 
                        else { rombix(a*rombAngle,b*rombAngle, rombSize); }
                    }
                }
            }
        }
    }
}
//the top half of the aforementioned triangle. 
//Pieces are rotated 180 degrees to fit inside a 3d printer bed better
//Then gets mirrored about the y axis and translated down to form a complete square
for(a=[1 : 1 : rombCount*2]) { 
    for(b=[1+rombCount: 1 : rombCount*2-a]) {
        translate([ //floor() hack to keep it square for decimal values of rombCount
        (a-(2*floor(rombCount))/2-1)*rombSize*(-2-rombSpace)+rombSize,  
        (b-rombSpace*3)*rombSize*(2+rombSpace)-rombCount*rombSize*2, 0]) {
            linear_extrude(height = rombHeight+rombFiletSize, center = false) {
                offset(r=rombFiletSize) { 
                    offset(r=-rombFiletSize) {
                        rotate([0,0,180]){
                            rombix(a*rombAngle,(b*rombAngle),rombSize); 
                        }
                    }
                }
            }
        }
    }
}
}

/*
generates the bed.
*/
if (generateBed) {
color("#666666")
translate([-rombCount*rombSize,rombCount*rombSize, rombHeight]) {
    union() {
        linear_extrude(rombHeight, center=false) {
            difference(){
                offset(r=2+innerBedOffset) {
                    circle(r=tan(90-(90/rombCount))*rombSize, $fn=rombCount*4);
                }
                offset(r=innerBedOffset) circle(r=tan(90-(90/rombCount))*rombSize, $fn=rombCount*4);
            }
        }
        translate([0,0,-rombHeight+eps]) {
            linear_extrude(rombHeight, center=false) {
                offset(r=2+innerBedOffset) {
                    circle(r=tan(90-(90/rombCount))*rombSize, $fn=rombCount*4);
                }
            }
        }
    }
}
}
/*
rombix() draws a rombix using the parallelogram function. Supplied two angles
for which it draws two rhombuses with, and then it unions them together. 
This module creates a 2d shape.
*/
module rombix(angle1, angle2, size) {
    union() { //seems to join them just fine without eps offset
              //doesn't draw a rhombus if it's too thin
        if ((angle1%180 >= eps) || (angle1%180 <= -eps)) {
            rhombus(angle1,size);
        }
        if ((angle2%180 >= eps) || (angle2%180 <= -eps)) {
            rhombus(angle2+180,size);
        }
    }
}

/*
Returns the width of a rombix. Not used. 
*/
function rombixWidth(angle1, angle2, size) = 
    abs(sin(angle2))*size + abs(sin(angle1))*size;

/*
creates a rhombus of a given angle and size. It is 2d. 
providing an angle greater than 180 degrees draw a rombus in the negative x plane
*/
module rhombus(angle, size) {
    polygon([ 
        [0,0], 
        [cos(angle)*size, sin(angle)*size],
        [size+cos(angle)*size, sin(angle)*size],
        [size,0],     
    ]);
}
