# Rombix Generator

OpenSCAD code that generates Rombix tiles, invented and patented by Alan H. Schoen in 1997.
You can buy a copy of this puzzle here, made out of laser-cut acryllic: http://www.gamepuzzles.com/esspoly3.htm

The STL file is a set of 36 tiles ready to be printed on a 3d printer with a square bed. rombix-bed.stl is an accompanying bed for holding the pieces. I recommend scaling this STL in your slicer according to the tolerances of the fitted parts. 

The code generates any n^2 set of rombix tiles that will completely fill an n-gon with 4*n sides. 


# 

A set of rombix pieces are defined in the following way:

Take N rombuses whose corner is of N even divisions between 0 and 90 degrees.

Join any subset of one or two of these rombuses along a side 

Do this for all combinations of N rombuses

Discard all duplicates that results from either rotations or reflections

TODO:
With some refactoring of the code it may be possible to fill an n-gon with 2*n sides. I've yet to play around with this. 

# You can use the openSCAD file to change any number of parameters, including:

The fillet radius

Number of tiles

Size of the tiles

Spacing between the tiles

